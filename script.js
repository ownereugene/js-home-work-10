function tabsDisplay() {
  const tabs = Array.from(document.querySelectorAll(".tabs-title"));
  for (let key in tabs) {
    tabs[key].addEventListener("click", (event) => {
      let target = event.currentTarget;

      for (let key in tabs) {
        tabs[key].classList.remove("active");
      }

      target.classList.add("active");

      //
      let targetNode = target.dataset.menu;
      let nodeNoDisplay = Array.from(
        document.querySelectorAll(".tabs-content li")
      );
      for (let key in nodeNoDisplay) {
        nodeNoDisplay[key].dataset.visibility = "none";
      }
      let nodeToDisplay = document.querySelector(
        `.tabs-content [data-menu='${targetNode}']`
      );
      nodeToDisplay.dataset.visibility = "display";
      //
    });
  }
}

tabsDisplay();

